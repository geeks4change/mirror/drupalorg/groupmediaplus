<?php

namespace Drupal\groupmediaplus;

use Drupal\Component\Render\FormattableMarkup;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Site\Settings;
use Drupal\group\Entity\GroupContent;
use Drupal\group\Entity\GroupContentType;
use Drupal\group\Entity\GroupInterface;
use Drupal\media_library\MediaLibraryState;

/**
 * Class GroupIdTools
 *
 * @internal
 */
class GroupIdTools {

  /**
   * Get and spread group IDs.
   *
   * @todo Refactor this conceptually.
   *
   * @param $debugTitle
   * @param \Drupal\Core\Form\FormStateInterface|null $formState
   * @param null $form
   *
   * @return mixed|null|string[]
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public static function getAndSpreadGroupIds($debugTitle, FormStateInterface $formState = NULL, &$form = NULL) {
    self::log("$debugTitle - Get&spread group IDs");
    // @see \Drupal\groupmediaplus\EventSubscriber\EntityBrowserSubscriber::alterDisplayData
    $groupIds = self::extractGroupIdsFromRouteMatch()
      ?? self::getGroupIdsFromOurQueryParameter()
      ?? self::getGroupIdsFromFormState($formState)
      ?? self::getGroupIdsFromOriginalPathQueryParameter()
      ?? self::getGroupIdsFromRefererHeader()
      ?? self::getGroupIdsFromEntityBrowserState()
      ?? self::logIdsNotFound();

    if (isset($groupIds)) {
      self::setGroupIdsToFormState($formState, $groupIds);
      self::setGroupIdsToOurQueryParameter($groupIds);
      self::setGroupIdsToEntityBrowserState($groupIds);

      // This is tricky: For the first popup page of the entity embed form, we
      // get the group ids from the referer. Clicking on other "tabs" will lose
      // the informative referer. Set entity browser widget context to the
      // rescue!
      // @todo This seems not to work.
      if (isset($form)) {
        if ($form['entity_browser']['#type'] ?? NULL === 'entity_browser') {
          $form['entity_browser']['#widget_context']['groupmediaplus_group_ids'] = $groupIds;
        }
      }
    }
    return $groupIds;
  }

  /**
   * @return null|string[]
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public static function getGroupIdsFromOriginalPathQueryParameter() {
    $ids = self::extractGroupIdsFromPath(\Drupal::request()->query->get('original_path'));
    self::logIds('Found on original path', $ids);
    return $ids;
  }

  /**
   * @return null|string[]
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public static function getGroupIdsFromRefererHeader() {
    $ids = self::extractGroupIdsFromPath(\Drupal::request()->headers->get('referer'));
    self::logIds('Found on referer :-(', $ids);
    return $ids;
  }

  /**
   * @return mixed
   */
  public static function getGroupIdsFromOurQueryParameter() {
    $ids = \Drupal::request()->query->get('groupmediaplus_group_ids');
    self::logIds('Found on our query parameter', $ids);
    return $ids;
  }

  /**
   * Get group IDs from entity browser state.
   *
   * @see \Drupal\entity_browser\DisplayBase::displayEntityBrowser
   */
  public static function getGroupIdsFromEntityBrowserState() {
    $uuid = \Drupal::request()->query->get('uuid');
    if ($uuid) {
      /** @var \Drupal\Core\KeyValueStore\KeyValueStoreExpirableInterface $selectionStorage */
      $selectionStorage = \Drupal::service('entity_browser.selection_storage');
      $data = $selectionStorage->get($uuid);
      $ids = $data['widget_context']['groupmediaplus_group_ids'] ?? NULL;
      self::logIds('Found on entity_browser stored widget context', $ids);
      return $ids;
    }
  }

  /**
   * Get group ids from referer. An ugly un-robust hack.
   *
   * @param string $path
   * @param string[] $entityTypeIds
   *
   * @return string[]|null
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public static function extractGroupIdsFromPath($path, $entityTypeIds = NULL) {
    $ids = NULL;

    if ($path) {
      /** @var \Symfony\Component\Routing\RouterInterface $router */
      $router = \Drupal::service('router.no_access_checks');
      try {
        $parameters = $router->match($path);
      } catch (\Exception $e) {
        return [];
      }
      $ids = self::extractGroupIdsFromRouteParameters($parameters, $entityTypeIds);
    }
    return $ids;
  }

  /**
   * Extract group IDs from route match.
   *
   * @param \Drupal\Core\Routing\RouteMatchInterface|null $routeMatch
   *
   * @return string[]|null
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected static function extractGroupIdsFromRouteMatch(RouteMatchInterface $routeMatch = NULL) {
    if (!$routeMatch) {
      $routeMatch = \Drupal::routeMatch();
    }
    return self::extractGroupIdsFromRouteParameters($routeMatch->getParameters()->all());
  }

  /**
   * Get group IDs from route parameters.
   *
   * @param array $parameters
   * @param string[]|null $entityTypeIds
   *
   * @return string[]|null
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected static function extractGroupIdsFromRouteParameters(array $parameters, array $entityTypeIds = NULL) {
    $ids = NULL;
    if (isset($parameters['group']) && ($group = $parameters['group']) && $group instanceof GroupInterface) {
      $groupId = $group->id();
      $ids = [$groupId => $groupId];
      self::logIds('Found group parameter on path', $ids);
    }
    if (!isset($entityTypeIds)) {
      $entityTypeIds = array_keys(self::groupContentEntityTypeOptions());
    }
    foreach ($entityTypeIds as $entityTypeId) {
      if (isset($parameters[$entityTypeId]) && ($entity = $parameters[$entityTypeId]) && $entity instanceof ContentEntityInterface) {
        $ids = self::combineIdsOrNull($ids, self::getGroupIdsFromEntity($entity));
      }
    }
    return $ids;
  }


  /**
   * 2: Get group ID from (popup!) form state (or Referer =:-()
   *
   * @param \Drupal\Core\Form\FormStateInterface $formState
   *
   * @return string[]|null
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public static function getGroupIdsFromFormState(FormStateInterface $formState = NULL) {
    if (!$formState) {
      return NULL;
    }
    $formId = $formState->getBuildInfo()['form_id'];
    $ids = NULL;

    // Handle entity browser widget with help from
    // @see groupmediaplus_field_widget_form_alter
    if ('entity_browser_' === substr($formId, 0, 15)) {
      $ids = $formState->get([
        'entity_browser',
        'widget_context',
        'groupmediaplus_group_ids'
      ]);
      self::logIds('Found on entity_browser form state', $ids);
    }
    // Handle media library widget with help from
    // @see groupmediaplus_field_widget_form_alter
    elseif ('media_library_add_form' === $formId) {
      $mediaLibraryState = $formState->get('media_library_state');
      if ($mediaLibraryState instanceof MediaLibraryState) {
        $ids = $mediaLibraryState->get('groupmediaplus_group_ids') ?? NULL;
        self::logIds('Found on media_library form state', $ids);
      }
    }
    // @fixme Wontwork.
    // EntityEmbedDialog is opened via
    // @see entity_embed/js/plugins/drupalentity/plugin.js:142
    // so we only can extract referer.
    // @see https://www.drupal.org/project/entity_embed/issues/3065983
    elseif ('entity_embed_dialog' === $formId) {
      $referer = \Drupal::request()->headers->get('referer', '');
      $ids = self::extractGroupIdsFromPath($referer);
      self::logIds('Found on entity_embed form state', $ids);
    }
    return $ids;
  }

  /**
   * Get entity group IDs, keyed by group ID.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *
   * @return array
   */
  public static function getGroupIdsFromEntity(ContentEntityInterface $entity) {
    if (is_null($entity->id())) {
      return NULL;
    }
    $groupIds = [];
    /** @var \Drupal\group\Entity\GroupContentInterface $groupContent */
    foreach (GroupContent::loadByEntity($entity) as $groupContent) {
      $groupId = $groupContent->getGroup()->id();
      $groupIds[$groupId] = $groupId;
    }
    self::logIds('Found on entity ' . $entity->getEntityTypeId() . ':' . $entity->id(), $groupIds);
    return $groupIds;
  }

  /**
   * @param $groupIds
   */
  public static function setGroupIdsToOurQueryParameter($groupIds) {
    \Drupal::request()->query->set('groupmediaplus_group_ids', $groupIds);
  }

  /**
   * Set group IDs to entity browser state.
   *
   * @see \Drupal\entity_browser\DisplayBase::displayEntityBrowser
   */
  public static function setGroupIdsToEntityBrowserState($groupIds) {
    $uuid = \Drupal::request()->query->get('uuid');
    if ($uuid) {
      /** @var \Drupal\Core\KeyValueStore\KeyValueStoreExpirableInterface $selectionStorage */
      $selectionStorage = \Drupal::service('entity_browser.selection_storage');
      $data = $selectionStorage->get($uuid);
      $data['widget_context']['groupmediaplus_group_ids'] = $groupIds;
      $selectionStorage->setWithExpire($uuid, $data, Settings::get('entity_browser_expire', 21600));
    }
  }

  /**
   * @param \Drupal\Core\Form\FormStateInterface $formState
   * @param $groupIds
   */
  public static function setGroupIdsToFormState(FormStateInterface $formState = NULL, $groupIds) {
    if ($formState) {
      $formState->set('groupmediaplus_group_ids', $groupIds);
    }
  }

  /**
   * Get all content entity type options.
   *
   * @return array
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public static function groupContentEntityTypeOptions() {
    $entityTypeIds = [];
    /** @var \Drupal\group\Entity\GroupContentTypeInterface $groupContentType */
    foreach (GroupContentType::loadMultiple() as $groupContentType) {
      $entityTypeId = $groupContentType->getContentPlugin()->getEntityTypeId();
      /** @var \Drupal\Core\Entity\EntityTypeInterface $entityType */
      $entityType = \Drupal::entityTypeManager()->getDefinition($entityTypeId);
      $entityTypeIds[$entityTypeId] = $entityType->getLabel();
    }
    return $entityTypeIds;
  }

  /**
   * Combine ids.
   *
   * @param array|null $ids1
   * @param array|null $ids2
   *
   * @return array|null
   */
  public static function combineIdsOrNull($ids1, $ids2) {
    if (is_null($ids1) && is_null($ids2)) {
      return NULL;
    }
    else {
      return array_merge($ids1 ?? [], $ids2 ?? []);
    }
  }

  /**
   * Log ids.
   *
   * @param string $title
   * @param array|null $ids
   */
  public static function logIds($title, $ids) {
    if (isset($ids)) {
      $tArgs = [
        '@title' => $title,
        '@ids' => isset($ids) ? implode('+', $ids) : 'NULL',
      ];
      $message = new FormattableMarkup('@title: @ids', $tArgs);
      self::log($message);
    }
  }

  public static function logIdsNotFound() {
    self::log('IDs NOT found.');
  }

  /**
   * @param $message
   */
  public static function log($message) {
    $debug = \Drupal::config('groupmediaplus.settings')->get('debug');
    if ($debug) {
      $message1 = new FormattableMarkup("GMPDbg: @message", ['@message' => $message]);
      \Drupal::messenger()->addWarning($message1, TRUE);
    }
  }

}
