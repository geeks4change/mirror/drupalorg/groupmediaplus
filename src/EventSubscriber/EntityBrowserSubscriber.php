<?php

namespace Drupal\groupmediaplus\EventSubscriber;

use Drupal\entity_browser\Events\AlterEntityBrowserDisplayData;
use Drupal\entity_browser\Events\Events;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class EntityBrowserSubscriber implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      Events::ALTER_BROWSER_DISPLAY_DATA => 'alterDisplayData',
    ];
  }

  /**
   * ALter display data: Add group IDs to entity browser query.
   *
   * @param \Drupal\entity_browser\Events\AlterEntityBrowserDisplayData $displayData
   *
   * @see \Drupal\entity_browser\Plugin\EntityBrowser\Display\Modal::displayEntityBrowser
   * @see \Drupal\entity_browser\Plugin\EntityBrowser\Display\IFrame::displayEntityBrowser
   */
  public function alterDisplayData(AlterEntityBrowserDisplayData $displayData) {
    $groupIds = $displayData->getFormState()->get('groupmediaplus_group_ids');
    if ($groupIds) {
      $data = $displayData->getData();
      $data['query_parameters']['query']['groupmediaplus_group_ids'] = $groupIds;
      $displayData->setData($data);
    }
  }

}
