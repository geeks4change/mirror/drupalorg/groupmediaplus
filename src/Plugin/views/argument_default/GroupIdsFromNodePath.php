<?php

namespace Drupal\groupmediaplus\Plugin\views\argument_default;

use Drupal\Core\Form\FormStateInterface;
use Drupal\groupmediaplus\GroupIdTools;
use Drupal\token\Token;
use Drupal\views\Plugin\views\argument_default\ArgumentDefaultPluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Default argument plugin to extract group IDs. The name is legacy.
 *
 * @ViewsArgumentDefault(
 *   id = "groupmediaplus_groups_from_node_path",
 *   title = @Translation("Group IDs using ANY hint from path.")
 * )
 */
class GroupIdsFromNodePath extends ArgumentDefaultPluginBase {

  /** @var \Drupal\token\TokenInterface */
  protected $tokenService;

  /**
   * GroupIdsFromPathContext constructor.
   * @param array $configuration
   * @param $plugin_id
   * @param $plugin_definition
   * @param \Drupal\token\Token $tokenService
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, Token $tokenService) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->tokenService = $tokenService;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('token')
    );
  }

  /**
   * @inheritDoc
   */
  protected function defineOptions() {
    $options = parent::defineOptions();
    $options['entity_type'] = ['default' => []];
    return $options;
  }

  /**
   * @inheritDoc
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);
    $form['entity_types'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('The entity type'),
      '#description' => $this->t('Extracts the entity\'s groups from a path like /node/42/edit.'),
      '#options' => GroupIdTools::groupContentEntityTypeOptions(),
      '#default_value' => $this->options['entity_types'],
    ];
  }

  /**
   * {@inheritdoc}
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getArgument() {
    $groupIds = GroupIdTools::getAndSpreadGroupIds('Get views argument');
    $return = isset($groupIds) ? implode('+', $groupIds) : NULL;
    return $return;
  }

}
