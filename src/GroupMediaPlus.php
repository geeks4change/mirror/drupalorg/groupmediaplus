<?php

namespace Drupal\groupmediaplus;

use Drupal\Core\Entity\ContentEntityFormInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\group\Entity\GroupInterface;
use Drupal\media_library\MediaLibraryState;

class GroupMediaPlus {

  /**
   * @param $form
   * @param \Drupal\Core\Form\FormStateInterface $formState
   * @param $formId
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public static function hookFormAlter(&$form, FormStateInterface $formState, $formId) {
    if (
      in_array($formId, ['media_library_add_form', 'entity_embed_dialog'])
      || 'entity_browser_' === substr($formId, 0, 15)
    ) {
      GroupIdTools::getAndSpreadGroupIds("hookFormAlter:$formId", $formState, $form);
    }
  }

  /**
   * 1: Add group IDs from form state to widget.
   *
   * @param $element
   * @param \Drupal\Core\Form\FormStateInterface $formState
   * @param $widgetContext
   */
  public static function transferGroupIdsFromFormStateToWidget(&$element, FormStateInterface $formState, &$widgetContext) {
    $isEntityBrowserWidget = $element['entity_browser']['#type'] ?? NULL === 'entity_browser' && isset($element['entity_browser']['#widget_context']);
    $mediaLibraryState = $element['media_library_open_button']['#media_library_state'] ?? NULL;
    $isMediaLibraryWidget = $mediaLibraryState instanceof MediaLibraryState;
    $isRelevant = $isEntityBrowserWidget || $isMediaLibraryWidget;
    if (!$isRelevant) {
      return;
    }
    $groupIds = [];
    // @see \Drupal\group\Entity\Controller\GroupContentController::createForm
    $group = $formState->get('group');
    if ($group instanceof GroupInterface) {
      $groupIds = [$group->id()];
      GroupIdTools::logIds('FormStateToWidget: Found group on form state', $groupIds);
    }
    $formObject = $formState->getFormObject();
    if ($formObject instanceof ContentEntityFormInterface) {
      /** @var ContentEntityInterface $entity */
      $entity = $formObject->getEntity();
      $groupIdsFromEntity = GroupIdTools::getGroupIdsFromEntity($entity);
      GroupIdTools::logIds('FormStateToWidget: Found group for entity ' . $entity->getEntityTypeId() . ':' . $entity->id(), $groupIdsFromEntity);
      $groupIds = GroupIdTools::combineIdsOrNull($groupIds, $groupIdsFromEntity);
    }

    if (isset($groupIds)) {
      // Entity browser.
      // @see \Drupal\entity_browser\Plugin\Field\FieldWidget\EntityReferenceBrowserWidget::formElement
      if ($isEntityBrowserWidget) {
        $widgetContext =& $element['entity_browser']['#widget_context'];
        $widgetContext['groupmediaplus_group_ids'] = $groupIds;
        GroupIdTools::log('FormStateToWidget: Set entity_browser widget context in form');
      }
      // Media library.
      // @todo We should not mess with this internal class. Sigh.
      elseif ($isMediaLibraryWidget) {
        /** @var \Drupal\media_library\MediaLibraryState $mediaLibraryState */
        $mediaLibraryState->set('groupmediaplus_group_ids', $groupIds);
        GroupIdTools::log('FormStateToWidget: Set media_library state');
      }
    }
  }

}
