<?php

namespace Drupal\groupmediaplus_upload;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\group\Entity\Group;
use Drupal\group\Entity\GroupContent;
use Drupal\group\Entity\GroupContentType;
use Drupal\group\Entity\GroupInterface;
use Drupal\group\Entity\Storage\GroupContentStorage;
use Drupal\groupmediaplus\GroupMediaPlus;
use Drupal\media\MediaInterface;
use Drupal\media_library\MediaLibraryState;

class Hooks {

  /**
   * Implements hook_form_alter().
   *
   * @todo Care for new entities that get group via pseudofield.
   * If we e.g. add a media item to a new node, save the media as unpublished
   * and after saving the node publish the media item and set groups.
   * We might need a lot of voodoo to get the uuid of the node and e.g. memorize
   * it in the state api.
   * Hopefully something better will happen meanwhile in core.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public static function hookFormAlter(&$form, FormStateInterface $formState, $formId) {
    $groupIds = $formState->get('groupmediaplus_group_ids');
    if (isset($groupIds)) {
      // As we have no suitable hook, we use this hack.
      array_unshift($form['#submit'], [SubmitStatus::class, 'on']);
      array_push($form['#submit'], [SubmitStatus::class, 'off']);
    }
  }

  /**
   * Implements hook_entity_insert().
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public static function hookEntityInsert(EntityInterface $entity) {
    if ($entity instanceof MediaInterface && ($formState = SubmitStatus::getFormState())) {
      self::submitMedia($entity, $formState);
    }
  }

  /**
   * Submit media group content.
   *
   * @param \Drupal\media\MediaInterface $media
   * @param \Drupal\Core\Form\FormStateInterface $formState
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public static function submitMedia(MediaInterface $media, FormStateInterface $formState) {
    $groupIds = $formState->get('groupmediaplus_group_ids');
    foreach ($groupIds as $groupId) {
      $group = Group::load($groupId);
      if ($groupContentType = self::getApplicableGroupContentType($media, $group)) {
        $groupContent = GroupContent::create([
          'type' => $groupContentType->id(),
          'gid' => $groupId,
          'entity_id' => $media->id(),
          'label' => $media->label(),
        ]);
        $groupContent->save();
      }
    }
  }

  /**
   * Get applicable group content type.
   *
   * @todo Move to utility class.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity.
   * @param \Drupal\group\Entity\GroupInterface $group
   *   The group.
   * @return \Drupal\group\Entity\GroupContentTypeInterface|null
   *   The (unique) group content type, or null.
   */
  protected static function getApplicableGroupContentType(EntityInterface $entity, GroupInterface $group) {
    /** @var \Drupal\group\Entity\GroupContentTypeInterface $groupContentType */
    foreach (GroupContentType::loadByEntityTypeId($entity->getEntityTypeId()) as $groupContentType) {
      if (
        $groupContentType->getGroupTypeId() === $group->getGroupType()->id()
        && $groupContentType->getContentPlugin()->getEntityBundle() === $entity->bundle()
      ) {
        return $groupContentType;
      }
    }
    return NULL;
  }

}
